import paho.mqtt.client as paho  # mqtt library
import json
import logging
from tb_rest_client.rest_client_ce import *
from tb_rest_client.rest import ApiException
from tb_rest_client.api.api_ce import *
import time
import random
import threading

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def crearSensor(name, typeSensor):
	# ThingsBoard REST API URL
	url = "http://localhost:8080"

	# Default Tenant Administrator credentials
	username = "tenant@thingsboard.org"
	password = "tenant"

	ACCESS_TOKEN = ''

	# Creating the REST client object with context manager to get auto token refresh
	with RestClientCE(base_url=url) as rest_client:
		try:
			# Auth with credentials
			rest_client.login(username=username, password=password)

			# creating a Device
			device = Device(name=name, type=typeSensor)
			device = rest_client.save_device(device)
			controller = DeviceControllerApi(api_client=rest_client.api_client)
			deviceCrendential = controller.get_device_credentials_by_device_id_using_get(device.id)
			ACCESS_TOKEN = deviceCrendential.credentials_id

			logging.info(" Device was created:\n%r\n", device)

		except ApiException as e:
			logging.exception(e)

	return ACCESS_TOKEN

def mandarDatos(THINGSBOARD_HOST, ACCESS_TOKEN, PORT=1883):

	INTERVAL=10

	sensor_data = {'temperature': 25.0, 'humedad': 0.0}
	next_reading = time.time()

	client = paho.Client()# Set access token
	client.username_pw_set(ACCESS_TOKEN)# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
	client.connect(THINGSBOARD_HOST, PORT, 60)
	client.loop_start()

	try:
		while True:
			print(sensor_data);
			#sensor_data['latitude'] =  random.uniform(42.67,42.68) #temperatura mediterranea
			#sensor_data['longitude'] =  random.uniform(-0.04,-0.05) #0-100 humedad
			sensor_data['temperature'] = random.uniform(-15.0, 45)
			sensor_data['humedad'] = random.uniform(0.0, 100)
			# Sending humidity and temperature data to ThingsBoard
			client.publish('v1/devices/me/telemetry', json.dumps(sensor_data), 1)
			next_reading += INTERVAL
			sleep_time = next_reading-time.time()
			if sleep_time > 0:
				time.sleep(sleep_time)
	except KeyboardInterrupt:
		pass
		client.loop_stop()
	client.disconnect()
	return


name = ''
ACCESS_TOKENS = []
i = 0
while name != 'q':

	name = input("Enter name of sensor or q to finish: ")
	if name != 'q':
		typeSensor = input("Enter type of sensor: ")

		ACCESS_TOKENS.append(crearSensor(name, typeSensor))
		i += 1

threads = list()
for x in range(i):
	t = threading.Thread(target=mandarDatos, args=('localhost', ACCESS_TOKENS[x],))
	threads.append(t)
	t.start()
